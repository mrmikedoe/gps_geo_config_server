FROM gps-registry:5000/gps-base:latest
RUN mkdir -p /opt/expedia/config-server

WORKDIR /opt/expedia/config-server

COPY target/*.jar /opt/expedia/config-server/config-server.jar

EXPOSE 8888

CMD ["java" , "-Dserver.port=8888", "-Dspring.cloud.consul.host=${CONSUL_HOST}", "-Dspring.cloud.consul.discovery.preferIpAddress=true", "-jar","/opt/expedia/config-server/config-server.jar"]
 
